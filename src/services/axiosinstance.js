import axios from 'axios'
// import { config } from 'dotenv'
// config()

const axiosInstance = axios.create({
  baseURL: 'https://kingwun-money-manager.herokuapp.com',
  timeout: 10000,
})
axiosInstance.interceptors.request.use(
  function(config) {
    const token = localStorage.getItem('money-manager-jwt') || ''
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  function(err) {
    return Promise.reject(err)
  },
)
export default axiosInstance
