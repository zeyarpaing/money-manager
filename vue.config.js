module.exports = {
  publicPath: './',
  pwa: {
    name: 'Money Manager',
    themeColor: '#4c5cb3',
    msTileColor: '#4c5cb3',
    display: 'standalone',
    appleMobileWebAppCache: 'yes',
    manifestOptions: {
      background_color: '#4050a0',
    },
  },
}
